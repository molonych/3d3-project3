from peer import *
import time
import random as rnd

def main():

    """
    Add cars and peers to network
    """
    quit = False
    peers = []

    # Let the simulation know which vehicles you want to
    # instantiate on each road (1 -> 9)
    while not quit:
        vehicle = input('Are you on a car or a bike? [c/b] ')
        road = input('Are you on road 1 or road 2? [between 1 and 9] ')
        peers.append(Peer(vehicle=vehicle, road=road, 
                        rating=[rnd.randint(6,9), rnd.randint(6,9), rnd.randint(13, 17), rnd.randint(1, 9)])) 
        ans = input('Would you like to add another peer? [y/n]:  ')
        if ans == 'n':
            quit = True
        elif ans != 'y':
            raise ValueError('You didnt say yes or no')
    
    """
    Exchange messages
    """
    # initialise sockets, have to use seperate thread for each car 
    # since we're simulating multicasting on a laptop
    for i in range(0, len(peers)):
            peers[i].start()

    for i in range(0,5):
        # all bikes send multicast message to group
        print(f'\n')
        for j in range(0, len(peers)):
            peers[j].send()

        # send messages every two seconds
        time.sleep(2)

        # Move bikes to another road
        if i%2 == 0:
            print("\nAll bikes moving to one road over\n")
            for k in range(0, len(peers)):
                if peers[k].vehicle == 'b':
                    # update port and ip numbers
                    peers[k].update()
            ans = input("would you like to update your rating? [yes/no] ")
            if ans == 'yes':
                # user inputs their rating
                id = int(input("which peer? (user id): "))
                # update your personal rating
                done = False
                while not done:
                    for l in range(len(peers)):
                        print(peers[l].user_id)
                        if peers[l].user_id == id:
                            idx = l
                            print('found one')
                            done = True
                    if not done:
                        id = input('enter a valid peer id: ')
                peers[idx].personal_rating_dir['busy'] = input("business?: ")
                peers[idx].personal_rating_dir['safety']  = input("safety?: ")
                peers[idx].personal_rating_dir['hour']  = input("hour?: ")
    
    # finished simulation, close sockets
    print('\nsimulation done -> closing sockets')
    for i in range(0,len(peers)):
            # join threads and close sockets
            peers[i].close()
            peers[i].join()


if __name__ == "__main__":
    main() 
