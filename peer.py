#from multiprocessing.sharedctypes import Value
import socket
import struct
import threading
import select
import ctypes
import numpy as np


class Peer(threading.Thread):

    def __init__(self, vehicle, road, rating):
        # Initialise threading
        super(Peer, self).__init__()
        self.vehicle = vehicle
        self.road = int(road)

        self.personal_rating = rating
        self.recieved_table = np.zeros([2,9])

        self.personal_rating_dir = {
                        # length of message -> 14 bits long 
                        'safety': rating[0], # rating from 1 to 10 - potholes, bad turns etc.
                        'busy': rating[1], # rating from 1 to 10 - busy road, cars parked in cycle lane etc.    
                        'hour': rating[2] # time you sent the info
                    }

        # giving all roads same rating for the sake of simulation
        self.global_table = [[i, 0, self.personal_rating_dir] for i in range(1,10)]
        self.init_global_rating = self.global_table
        
        print(self.global_table[0])
        if self.road < 1 or self.road > 9:
            raise ValueError('Enter a valid road number')

        # give port numbers and ip based on road
        self.ip = '224.1.1.' + road
        if self.road < 10:
            self.port = int('500' + road)
        elif self.road < 100:
            self.port = int('50' + road)
        elif self.road < 1000:
            self.port = int('5' + road)

        self.initSocket()

    def update(self):
        # increment port number by 1 since we're moving one road over
        self.road += 1
        self.port = self.port + 1
        self.ip = self.ip[:-1] + str(int(self.road))
        self.send_sock.close()
        self.rcv_sock.close()
        self.initSocket()
   
    
    def decodeMsg(self, msg):
        # extract info
        recieved_id = int(msg[0:11])
        safety = int(msg[11])
        busy = int(msg[12])
        hour = int(msg[13:15])
        vehicle = int(msg[-1])
        # put into a list
        msg = [recieved_id, safety, busy, hour, vehicle]
        return msg     

    def updatePersonalRating(self, rating):
        self.personal_rating_dir = {
                        'safety': rating[0], # rating from 1 to 10 - potholes, bad turns etc.
                        'cycle lane': rating[1], # binary: 1 -> cycle lane, 0 -> no cycle lane 
                        'busy': rating[2], # rating from 1 to 10 - busy road, cars parked in cycle lane etc.    
                        }

    def initSocket(self):
        # intialise UDP socket for multicast receiving
        self.rcv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.rcv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # use the group multicast port number and ip as data source
        self.rcv_sock.bind((self.ip, self.port))
        # Add receiver to multicast group
        mreq = struct.pack("4sl", socket.inet_aton(self.ip), socket.INADDR_ANY)
        self.rcv_sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        # set up socket for multicast sending
        self.send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.send_sock.settimeout(0.2)
        self.send_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)
    
    def send(self):
        # convert message to bytes before sending 
        rating = map(str, self.personal_rating)
        msg = str(self.user_id) + ''.join(rating) + '0'*(self.vehicle == 'b') + '1'*(self.vehicle == 'c')
        msg = bytes(msg.encode())
        # send message to multicast group
        self.send_sock.sendto(msg, (self.ip, self.port))
        if self.vehicle == 'b':
            print(f'Bike {self.user_id}: sending messages to vehicles with port {self.port} and ip {self.ip}')
        else:
            print(f'Car {self.user_id}: sending messages to vehicles with port {self.port}')
          
    def run(self):
        done = False
        self.user_id = self.get_id()
        while not done:
            # identify which car is listening and which bike it recieved a message from
            # first, check if we're recieving a message
            # wait one second to check if car                     
            try:
                rcv, _, _ = select.select([self.rcv_sock], [], [], 2.05)
                if rcv:
                    rcv_msg = self.rcv_sock.recv(4092)
                    rcv_msg = rcv_msg.decode()
                    rcv_msg = self.decodeMsg(rcv_msg)
                    # if a bike sent the message, let the car driver know there's a bike on the road
                    if rcv_msg[0] != self.user_id and self.vehicle == 'c':
                        if rcv_msg[-1] == 0:
                            print(f'Car {self.user_id}: message recived from user {rcv_msg[0]}, updating global rating\nBIKE ON ROAD')
                        else:
                            print(f'Car {self.user_id}: message recived from user {rcv_msg[0]}, updating global rating')
                    elif rcv_msg[0] != self.user_id and self.vehicle == 'b':
                        print(f'Bike {self.user_id}: message recived from user {rcv_msg[0]}, updating global rating')
                else:
                    print(f'Car {self.port}: no bike on road')
            except:
                done = True

    
    """
    Utility functions for closing threads and sockets
    """
    def get_id(self):
        # returns id of the respective thread
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def close(self):
        # First close socket 
        self.send_sock.close()
        self.rcv_sock.close()
        print(f'peer: {self.user_id} socket closed')
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
              ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')
